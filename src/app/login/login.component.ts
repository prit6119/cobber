import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersrvcService } from '../usersrvc.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb: FormBuilder, private usersvc: UsersrvcService, private router: Router) { }
  loginForm = this.fb.group({
    email: ['', Validators.required],
    password:  ['', Validators.required],
    remember: []
  })
  
  ngOnInit(): void {
  }

  login() {
    console.log("login form", this.loginForm.value)

    this.usersvc.login(this.loginForm.value).subscribe((data: any) => {
      console.log("login data", data)

     if(data.login_status){

        this.router.navigateByUrl('profile');
      }else{
        alert('Email or password is wrong.')
      } 
    })
  }
}
