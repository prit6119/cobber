import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersrvcService } from '../usersrvc.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(private usersvc: UsersrvcService, private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute) {
    this.entries = [];
  }
  name: any;


  sid: any;
  public entries: any;
  public ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.sid = params['sid'];
      let headers = new HttpHeaders({ 'authorization': 'Bearer ' + params['sid'] });
      this.http.get('http://49.12.217.12:5000/get_profile_angular', { headers: headers })
        .subscribe(result => {
          this.entries = result;
          console.log(result)
        });
    });
  }

  public create() {
    this.router.navigate(['/blog'], { 'queryParams': { 'sid': this.sid } });
  }

}




  

