import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UsersrvcService } from '../usersrvc.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private fb: FormBuilder, private usersvc: UsersrvcService, private router: Router) { }
  signupForm = this.fb.group({
    name: [''],
    gender:[''],
    email: [''],
    password: ['']
  })
  error: boolean = false;
  ngOnInit(): void {
  }
  signup() {
    let body = {
      name: this.signupForm.value.name,
      gender: this.signupForm.value.gender,
      email: this.signupForm.value.email,
      password: this.signupForm.value.password,
    }
    // localStorage.setItem('name', this.signupForm.value.name);
    // localStorage.setItem('email', this.signupForm.value.email);
    console.log("signupdata", body);



    this.usersvc.signup(body).subscribe((data: any) => {
      console.log("signupdata", data);
      if (data.signup_status) {
        this.router.navigateByUrl('login')
      } else {
        this.error = true;
      }

    })

  }
}
